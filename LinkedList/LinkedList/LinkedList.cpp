#include "iostream"
#include "fstream"
#include "ListRand.h"

int main()
{
	ListRand List;
	
	int Num;
	std::cout << "Input length of list: ";
	std::cin >> Num;
	std::cout << "The list length = " << Num << std::endl;
	for (int i = 0; i < Num; ++i)
	{
		List.PushBack(std::to_string(i));
	}

	List.SetRandomPointer();

	std::cout << "Serialize list" << std::endl;
	std::cout << List << std::endl;

	std::ofstream OutFile("../Result/SerializeSimpleListRand.txt");
	List.Serialize(OutFile);
	OutFile.close();

	std::cout << std::endl;

	std::ifstream InFile("../Result/SerializeSimpleListRand.txt");
	List.Deserialize(InFile);
	InFile.close();

	std::cout << "Deserialize list" << std::endl;
	std::cout << List << std::endl;

	std::cout << std::endl;
	return 0;
}
