#pragma once

#include "string"

struct ListNode {
	ListNode* Prev;
	ListNode* Next;
	ListNode* Rand; // произвольный элемент данного списка
	std::string Data;
};