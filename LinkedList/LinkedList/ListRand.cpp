#include "ListRand.h"
#include "fstream"
#include "iostream"
#include "random"
#include "unordered_map"

void ListRand::Serialize(std::ostream& Stream)
{
	if (Empty()) // Метод реализованый в классе ListRand
	{
		return;
	}

	Stream << Count << std::endl;
	std::unordered_map<ListNode*, int> OffsetRandNode;

	int NodeId = 0;
	for (auto i = Head; i != nullptr; i = i->Next) // Каждый i - это узел
	{
		Stream << i->Data << std::endl;
		OffsetRandNode.emplace( i, NodeId++ );
	}

	for (auto j = Head; j != nullptr; j = j->Next) // Каждый j - это узел
	{
		Stream << OffsetRandNode[j->Rand] << std::endl;
	}
}

void ListRand::Deserialize(std::istream& Stream)
{
	int Length = 0;
	Stream >> Length;
	if (Length <= 0)
	{
		return;
	}

	std::vector< ListNode*> OffsetRandNode;
	OffsetRandNode.reserve(Length);

	std::string Data;
	for (int i = 0; i < Length; ++i) // Каждый i - это узел
	{
		Stream >> Data;
		PushBack(Data); // Метод реализованый в классе ListRand
		OffsetRandNode.push_back(Tail);
	}

	int Id;
	for (auto j = Head; j != nullptr; j = j->Next) // Каждый j - это узел
	{
		Stream >> Id;
		j->Rand = OffsetRandNode[Id];
	}
}

bool ListRand::Empty()
{
	if (Count == 0)
	{
		std::cout << "Empty!!!" << std::endl;
		return true;
	}
	return false;
}

void ListRand::Clear()
{
	if (Empty())
	{
		return;
	}

	for (auto i = Head; i != nullptr; i = i->Next)
	{
		if (i->Prev != nullptr)
		{
			delete i->Prev;
		}
	}

	if (Tail)
	{
		delete Tail;
	}

	Head = Tail = nullptr;
	Count = 0;
}

void ListRand::PushBack(const std::string & Value)
{
	if (!Head)
	{
		Head = new ListNode();
		Head->Data = Value;
		Tail = Head;
		Head->Next = Head->Prev = nullptr;
	}
	else
	{
		Tail->Next = new ListNode();
		Tail->Next->Prev = Tail;
		Tail = Tail->Next;
		Tail->Next = nullptr;
		Tail->Data = Value;
	}

	++Count;
}

void ListRand::SetRandomPointer()
{
	if (Empty())
	{
		return;
	}

	std::vector< ListNode*> OffsetRandNode;
	OffsetRandNode.reserve(Count);

	for (auto i = Head; i != nullptr; i = i->Next)
	{
		OffsetRandNode.push_back(i);
	}

	std::random_device Random;
	std::uniform_int_distribution<int> UniformDistribution(0, Count - 1);
	for (auto i = Head; i != nullptr; i = i->Next)
	{
		int RandomId = UniformDistribution(Random);
		i->Rand = OffsetRandNode[RandomId];
	}
}

ListRand::~ListRand() 
{
	Clear();
}

std::ostream& operator<<(std::ostream& out,  ListRand& List)
{
	if (List.Empty())
	{
		out << "Empty!" << std::endl;
		return out;
	}

	for (auto i = List.Head; i != nullptr; i = i->Next)
	{
		out << "Data = " << i->Data << ": Random value = " << i->Rand->Data << std::endl;
	}
	return out;
}
