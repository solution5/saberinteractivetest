#pragma once

#include "string"
#include "ListNode.h"

class ListRand {
public:
	ListNode* Head = nullptr;
	ListNode* Tail = nullptr;
	int Count = 0;

public:
	// Метод сериализации
	void Serialize(std::ostream & Stream);
	// Метод десериализации
	void Deserialize(std::istream & Stream);

private:
	// Перегруженный оператор вывода
	friend std::ostream& operator<<(std::ostream& out, ListRand& List);

public:

	// Конструктор класса по-умолчанию
	ListRand() {}

	// Метод для проверки на пустой список
	bool Empty();

	// Метод для очистки списка
	void Clear();

	// Метод для добовления элемнета в список
	void PushBack(const std::string& Data);

	// Метод для создания случайных элементов списка
	void SetRandomPointer();

	//Деструктор класса
	~ListRand();
};