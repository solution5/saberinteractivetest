## Problem 1

Реализуйте функции сериализации и десериализации двусвязного списка, заданного следующим образом:
```C#

class ListNode
    {
 public ListNode Prev;
        public ListNode Next;
        public ListNode Rand; // произвольный элемент внутри списка
        public string Data;
    }


    class ListRand
    {
        public ListNode Head;
        public ListNode Tail;
        public int Count;

        public void Serialize(FileStream s)
        {
        }

        public void Deserialize(FileStream s)
        {
        }
    }

```
**Примечание:** сериализация подразумевает сохранение и восстановление полной структуры списка, включая взаимное соотношение его элементов между собой — в том числе ссылок на Rand элементы.

### Solution 1

Основное решение:

ListRand.cpp
```C++

void ListRand::Serialize(std::ostream& Stream)
{
	if (Empty()) // Метод реализованый в классе ListRand
	{
		return;
	}

	Stream << Count << std::endl;
	std::unordered_map<ListNode*, int> OffsetRandNode;

	int NodeId = 0;
	for (auto i = Head; i != nullptr; i = i->Next) // Каждый i - это узел
	{
		Stream << i->Data << std::endl;
		OffsetRandNode.emplace( i, NodeId++ );
	}

	for (auto j = Head; j != nullptr; j = j->Next) // Каждый j - это узел
	{
		Stream << OffsetRandNode[j->Rand] << std::endl;
	}
}

void ListRand::Deserialize(std::istream& Stream)
{
	int Length = 0;
	Stream >> Length;
	if (Length <= 0)
	{
		return;
	}

	std::vector< ListNode*> OffsetRandNode;
	OffsetRandNode.reserve(Length);

	std::string Data;
	for (int i = 0; i < Length; ++i) // Каждый i - это узел
	{
		Stream >> Data;
		PushBack(Data); // Метод реализованый в классе ListRand
		OffsetRandNode.push_back(Tail);
	}

	int Id;
	for (auto j = Head; j != nullptr; j = j->Next) // Каждый j - это узел
	{
		Stream >> Id;
		j->Rand = OffsetRandNode[Id];
	}
}

```
Я реализовал решение во все проекте и для примера сделал случайны список результат которого можно посмотреть в папке[Result](Result).