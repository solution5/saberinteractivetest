# SaberInteractiveTest

Решение задач из тестого задания для Saber Interactive

## Problem 1

Реализуйте функции сериализации и десериализации двусвязного списка, заданного следующим образом:
```C#

class ListNode
    {
 public ListNode Prev;
        public ListNode Next;
        public ListNode Rand; // произвольный элемент внутри списка
        public string Data;
    }


    class ListRand
    {
        public ListNode Head;
        public ListNode Tail;
        public int Count;

        public void Serialize(FileStream s)
        {
        }

        public void Deserialize(FileStream s)
        {
        }
    }

```
**Примечание:** сериализация подразумевает сохранение и восстановление полной структуры списка, включая взаимное соотношение его элементов между собой — в том числе ссылок на Rand элементы.

### Solution 1

Решение находится в папке [LinkedList](LinkedList) основное решение:

ListNode.cpp
```C++

void ListRand::Serialize(std::ostream& Stream)
{
	if (Empty()) // Метод реализованый в классе ListRand
	{
		return;
	}

	Stream << Count << std::endl;
	std::unordered_map<ListNode*, int> OffsetRandNode;

	int NodeId = 0;
	for (auto i = Head; i != nullptr; i = i->Next) // Каждый i - это узел
	{
		Stream << i->Data << std::endl;
		OffsetRandNode.emplace( i, NodeId++ );
	}

	for (auto j = Head; j != nullptr; j = j->Next) // Каждый j - это узел
	{
		Stream << OffsetRandNode[j->Rand] << std::endl;
	}
}

void ListRand::Deserialize(std::istream& Stream)
{
	int Length = 0;
	Stream >> Length;
	if (Length <= 0)
	{
		return;
	}

	std::vector< ListNode*> OffsetRandNode;
	OffsetRandNode.reserve(Length);

	std::string Data;
	for (int i = 0; i < Length; ++i) // Каждый i - это узел
	{
		Stream >> Data;
		PushBack(Data); // Метод реализованый в классе ListRand
		OffsetRandNode.push_back(Tail);
	}

	int Id;
	for (auto j = Head; j != nullptr; j = j->Next) // Каждый j - это узел
	{
		Stream >> Id;
		j->Rand = OffsetRandNode[Id];
	}
}

```

## Provlem 2

Составить BehaviourTree (в виде схемы) для NPC, который в спокойном состоянии патрулирует территорию, по окончанию каждого патруля (окончанием считается возвращение в стартовую точку) NPC может или заснуть на 2 минуты или продолжить патруль. Когда у NPC есть враг, он  атакует его, причем стрелять во врага может, только если у него есть патроны и враг на расстоянии меньше 30 метров. Если патронов нет, то NPC убегает от врага.

### Solution 2

Решение находится в папках [Behavior](Behavior):

2) BehaviourTree/Images/BehaviourTree.png

![BehaviourTree](BehaviourTree/Images/BehaviourTree.png)

**Примечание:** Есть пункты подчеркнутые пунктиром так как требуют уточнения у заказчика. К примеру стрельба не понятно когда прекращать стрельбу и что делать 
если персонаж сбежал. Но это элементы требующие просто уточнения.

А так же решениев в Unreal:

BehaviourTree/Images/BehaviourTreeUnreal.png

![BehaviourTreeUnreal](BehaviourTree/Images/BehaviourTreeUnreal.png)

**Примечание:** Некотрые элементы реализованы в Task и декораторах но суть кажется я смог передать.